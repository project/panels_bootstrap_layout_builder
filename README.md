<!-- @file Project Page -->
# Panels Bootstrap Layout Builder

This module is a Panels layout builder based on Twitter Bootstrap 2 & 3. It helps you to dynamically
build panels layouts based on the Bootstrap grid system.

# Getting Started:
## Step 1:
[Download Panels Bootstrap Layout Builder]: Download the module and place it in your site's modules directory:
-> sites/all/modules (usually)

## Step 2 (most people will need to do this):
[Bootstrap Framework]: http://getbootstrap.com/getting-started/#download

Download the Bootstrap framework from the above link using the "Download Bootstrap" button.
(Other buttons may also work but are out of scope for this README.)

-> sites/all/libraries/bootstrap

The directory structure should be:
-> sites/all/libraries/bootstrap/js
-> sites/all/libraries/bootstrap/fonts
-> sites/all/libraries/bootstrap/css

## Why You Need to do Step 2:
Even if your site's theme is a Bootstrap-based theme and loads the Bootstrap libraries, you probably use an admin
theme (such as the Seven theme). If the libraries are not loaded, the module will break, so this module attempts to
load the CSS from sites/all/libraries/bootstrap/css/bootstrap.min.css

If your administration theme is Bootstrap-based, then you can probably skip Step 2.

## Upgrading Issues:
Unfortunately, previous versions of this module did not set defaults to ensure the interface works.
With the latest version of this module, you can remedy this by editing your panels layout.

Click "Show Layout Designer".
Then, in "Canvas, Container, Rows, Columns, and Regions", click to edit the "Settings".
Then, "Save" each group of settings.
"Update and Save" the layout.

By doing all of these steps, the default settings should be re-established.

Make sure to do this before adjusting the column size settings; if you do not, they will be lost and
need to be re-entered.
